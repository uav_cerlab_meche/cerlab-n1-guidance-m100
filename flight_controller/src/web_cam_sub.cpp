#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>

using namespace cv;
using namespace std;
int key = 20;

int num = 1;
int folder = 0;

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_im;
  std::vector<int> compression_param;
  compression_param.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_param.push_back(3);

  try
    {
      cv_im = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
      cv::namedWindow("view");
      cv::imshow("view", cv_im->image);
      key = cv::waitKey(1);
      if((char)key == 'c')
	{
	  const string str =((( "images/test" + to_string(folder))  + to_string(num)) + ".png");
	  imwrite(str, cv_im->image, compression_param);
	  num++;
	}
    }
  catch(cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_listner");
  if(argc != 2)
    {
      ROS_ERROR("Invlaid number of argumemts");
      return -1;
    }else
    {
      folder = atoi(argv[1]);
    }
  ros::NodeHandle nh;
  ros::Subscriber sub;
 
  // while(ros::ok())
    {
      sub = nh.subscribe("guidance/left_image",1,&imageCallback);
      ros::spin();
      
    }
 
  cv::destroyWindow("view");
  return 0;
}
