#include <dji_sdk/drone.h>
#include <ros/ros.h>
#include <cstdlib>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

void getLocalPosition(dji_sdk::LocalPosition msg)
{
  ROS_INFO("X Pos: %f, Y Pos: %f, Z Pos: %f", msg.x, msg.y, msg.z);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "test");
  ros::NodeHandle nh;
  
  ros::Subscriber lp = nh.subscribe("dji_sdk/local_position", 10, &getLocalPosition);

  ros::spin();
  return 0;
}
