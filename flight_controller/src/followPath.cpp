#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <opencv2/highgui/highgui.hpp>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <guidance/OffsetData.h>
#include <geometry_msgs/TransformStamped.h>

using namespace DJI::onboardSDK;
using namespace cv;

ros::Subscriber sub1;
#define AS_PI 3.14159
void offsetDataReceived(const guidance::OffsetData& msg)
{
ROS_INFO("msg.dx_l: %f, msg.dx_r: %f, msg.slope_l: %f, msg.slope_r: %f\n", msg.dx_l, msg.dx_r, msg.slope_l, msg.slope_r);

//ROS_INFO("x: %f, y: %f, z: %f, r0: %f, r1: %f, r2: %f, r3: %f", msg.transform.translation.x, msg.transform.translation.y, msg.transform.translation.z, msg.transform.rotation.w, msg.transform.rotation.x, msg.transform.rotation.y, msg.transform.rotation.z);
}

int main(int argc, char** argv)
{

ros::init(argc, argv, "follow_path");
ros::NodeHandle nh;
DJIDrone* drone;
drone = new DJIDrone(nh);

//while(ros::ok())
  {
sub1 = nh.subscribe("/guidance/imu", 2, &offsetDataReceived); 
 ros::spin();
}


  return 0;
}
