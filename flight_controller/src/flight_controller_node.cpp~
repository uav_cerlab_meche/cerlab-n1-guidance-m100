#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <stdlib.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

using namespace DJI::onboardSDK;


class PathFollower
{
public :
  ros::NodeHandle nh;
  DJIDrone* drone;
  

public :

  PathFollower();
  ~PathFollower(void);
  void takeoff(void);
  void landing(void);
  void build_reduce_height(const double h);
  void gohome(void);
  void obtain_control(void);
  void release_control(void);
  dji_sdk::LocalPosition getCurrentPosition(void);
  void stablize_flight(const dji_sdk::LocalPosition pos);
  void navigate_to(const double x, const double y, const double z, const double yaw);

};

PathFollower::PathFollower()
{
  drone = new DJIDrone(nh);
}

PathFollower::~PathFollower()
{
  delete drone;
}

void PathFollower::obtain_control()
{
   drone->request_sdk_permission_control();
}

void PathFollower::release_control()
{
   drone->release_sdk_permission_control();
}

void PathFollower::takeoff()
{
   drone->takeoff();

}

void PathFollower::build_reduce_height(const double h)
{
 
  double dz = 0.0;
  double vz = 2.0, vzz = 0.2;
  
  while(ros::ok())
                {
		  ROS_INFO("dz: %f",dz);
		  dz = h - drone->local_position.z;
		  if(dz < 0)
		    {
		      vz = -2.0; vzz = -0.2;
		    }else
		    {
		      vz = 2.0 ; vzz = 0.2;
		    }
		  if( fabs(dz) < 2)
		    {
		  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_POSITION |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
                            0, 0, vzz, 0 );
                    usleep(20000);
		    }else
		    {
		  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_POSITION |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
                            0, 0, vz, 0 );
                    usleep(20000);
		    }

		    if(fabs(dz) < 0.1)
		      {
			break;
		      }
		    ros::spinOnce();
		    }
}

void PathFollower::landing()
{
   drone->landing();
}

void PathFollower::gohome()
{
   drone->gohome();
}

dji_sdk::LocalPosition PathFollower::getCurrentPosition()
{
  dji_sdk::LocalPosition pos;
  pos.ts = drone->local_position.ts;
  pos.x = drone->local_position.x;
  pos.y = drone->local_position.y;
  pos.z = drone->local_position.z;

  return pos;
}

void PathFollower::navigate_to(const double x, const double y, const double z, const double yaw)
{
  double dx = 0.0;
  double dy = 0.0;
  double dz = 0.0;

  double vx = 1, vy = 1, vz = 1;
  double vxx = 0.2, vyy = 0.2, vzz = 0.2;

  while(ros::ok())
    {
      dx = x - drone->local_position.x;
      dy = y - drone->local_position.y;
      dz = z - drone->local_position.z;
      if(dx < 0)
	{
	  vx = -1; vxx = -0.1;
	}else
	{
	  vx = 1; vxx = 0.1;
	}
      if(dy < 0)
	{
	  vy = -1; vyy = -0.1;
	}else
	{
	  vy = 1; vyy = 0.1;
	}
      if(dz < 0)
	{
	  vz = -1; vzz = -0.1;
	}else
	{
	  vz = 1; vzz = 0.1;
	}
      if( fabs(dx) < 1.0 && fabs(dy) < 1.0 && fabs(dz) < 1.0)
	{
        drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
				 vxx, vyy, vzz, yaw );
      usleep(20000);
	}else
	{
	          drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
				 vx, vy, vz, yaw );
      usleep(20000);
	}

      ROS_INFO("dx: %f, dy: %f, dz: %f", dx, dy, dz);
      if(fabs(dx) < 0.2 && fabs(dy) < 0.2 && fabs(dz) < 0.2)
	{
	  break;
	}
      ros::spinOnce();

      }
  }

void PathFollower::stablize_flight(const dji_sdk::LocalPosition pos)
{								
  /*ROS_INFO("stabalizing the flight...");
  dji_sdk::LocalPosition current_pos = drone->local_position;

  double dx = current_pos.x - pos.x;
  double dy = current_pos.y - pos.y;
  double dz = current_pos.z - pos.z;

  while(fabs(dx) <= 0.1 && fabs(dy) <= 0.1 && fabs(dz) <= 0.1)
    {
      drone->attitude_control(0x90, pos.x, pos.y, pos.z, 0);
      usleep(20000);

      current_pos = drone->local_position;
      dx = current_pos.x - pos.x;
      dy = current_pos.y - pos.y;
      dz = current_pos.z - pos.z;
      }*/
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "flight_controller");
 
  PathFollower pf;
  int temp32;
  int flag = 0;
  dji_sdk::LocalPosition pos;

  double x = 0, y = 0, z = 0, yaw = 0; //target location
  if(argc < 1)
    {
      return -1; 
    }else
    {
      x = atof(argv[1]); y = atof(argv[2]); z = atof(argv[3]); yaw = atof(argv[4]);
      ROS_INFO("x: %f, y: %f, z: %f, yaw: %f", x,y,z,yaw);
    }

  ROS_INFO("Welcome to CERLAB's Path Following Drone...!");
  ROS_INFO("Ready to receive commands...");
  
  while(ros::ok())
    { 
      temp32 = getchar();
      switch(temp32)
	{
	case 'C':
	  pos = pf.getCurrentPosition();
	  ROS_INFO("X: %f, Y: %f, Z: %f", pos.x, pos.y, pos.z);
	  flag = 0;
	  break;
	case 't':
	  pf.drone->takeoff();
	  pf.build_reduce_height(10.0);
	  flag = 0;
	  break;
	case 'l':
	  pf.landing();
	  flag = 1;
	case 'g':
	  pf.gohome();
	  flag = 1;
	  break;
	case 'o':
	  pf.obtain_control();
	  flag = 1;
	  break;
	case 'r':
	  pf.release_control();
	  flag = 1;
	  break;
	case 's':
	  pos = pf.getCurrentPosition();
	  ROS_INFO("X: %f, Y: %f, Z: %f", pos.x, pos.y, pos.z);
	  pf.stablize_flight(pos);
	  break;
	case 'n':
	  pos = pf.getCurrentPosition();
	  pf.navigate_to(x,y,z,yaw);
	  break;
	case 'L':
	  pf.drone->local_position_control(x,y,z,yaw);
	case 'q':
	  break;
	  }
     ros::spinOnce();
    }
  
 return 0;
}


