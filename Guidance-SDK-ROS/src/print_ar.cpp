#include <ros/ros.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <tf/tf.h>  
#include <tf/transform_datatypes.h>
#include <vector>

void printPose(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{   
		if(msg->markers.size() > 0)
		{
			ROS_INFO("x: %f, y: %f, z: %f, q0: %f, q1: %f, q2: %f, q3: %f", msg->markers[0].pose.pose.position.x, msg->markers[0].pose.pose.position.y, msg->markers[0].pose.pose.position.z, msg->markers[0].pose.pose.orientation.x, msg->markers[0].pose.pose.orientation.y, msg->markers[0].pose.pose.orientation.z, msg->markers[0].pose.pose.orientation.w);	
		}
		
		ROS_INFO("size: %d",(int)msg->markers.size());
    
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pose_subscriber");

    ros::NodeHandle nh;

    ros::Subscriber pose_sub = nh.subscribe("ar_pose_marker", 100, printPose);

    ros::spin();

    return 0;

}
