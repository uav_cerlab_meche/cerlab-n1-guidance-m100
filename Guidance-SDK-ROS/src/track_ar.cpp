/*#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <ar_track_alvar_msgs/AlvarMarker.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <guidance/OffsetData.h>
#include <geometry_msgs/TransformStamped.h>

using namespace DJI::onboardSDK;
#define AS_PI 3.141590118

ros::Subscriber sub1;
DJIDrone* drone;

double rad2deg(double angle)
{
  return (angle*180)/AS_PI;
}

double quat2yaw(double q0, double q1, double q2, double q3)
{
  return rad2deg(AS_PI - atan2(2*(q0*q3 + q1*q2), -1 + 2*(q2*q2 + q3*q3)));
}

class DJIPathFollower
{
public:
  ros::Subscriber sub1, sub2;
  ros::NodeHandle nh;
  DJIDrone* drone;
  guidance::OffsetData data;
  ar_track_alvar_msgs::AlvarMarker marker;
  int flag;
  double myYaw;
  char key;
   
public:
  DJIPathFollower();
  ~DJIPathFollower();
  void offsetData_callback(const guidance::OffsetData& msg);
  void arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg);
  void track_ar_marker(void);
  double get_currentyaw(void);
  void track_ar_yaw(void);

};

DJIPathFollower::DJIPathFollower()
{
  drone = new DJIDrone(nh);
  sub1 = nh.subscribe("/guidance/posoffset", 10, &DJIPathFollower::offsetData_callback, this);
  sub2 = nh.subscribe("/ar_pose_marker", 10, &DJIPathFollower::arPose_callback, this);
  key = 'a';
  flag = 0;
  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}

DJIPathFollower::~DJIPathFollower()
{
  delete drone;
}
void DJIPathFollower::offsetData_callback(const guidance::OffsetData& msg)
{
  this->data = msg;
}

void DJIPathFollower::arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg)
{   
	
	if(msg.markers.size() > 0)
	{
		this->marker = msg.markers[0];
		flag = 1;
	}else
	{
		flag = 0;
	}
		
}

double DJIPathFollower::get_currentyaw(void)
{
	return quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}
void DJIPathFollower::track_ar_yaw(void)
{
}

void DJIPathFollower::track_ar_marker(void)
{
	double x_pos = marker.pose.pose.position.x;
	double y_pos = marker.pose.pose.position.y;
	double z_pos = marker.pose.pose.position.z;
	double vy = 0.1, vz = 0.1, vx = 0.1;
	double tolerance = 0.5;
	if( flag == 1)
	{
		while(ros::ok())
		{
			ROS_INFO("x: %f y: %f z: %f", marker.pose.pose.position.x, marker.pose.pose.position.y, marker.pose.pose.position.z);
			if(marker.pose.pose.position.z >= 0.6)
			{
				vy = 0.1;
				vx = 0.1;
			}else if(marker.pose.pose.position.z <= 0.55)
			{
				vy = -0.1;
				vx = -0.1;
			}else
			{
					vx = 0;
			}
		
			/*if(fabs(marker.pose.pose.position.x) < tolerance)
			{
				vy = 0;
				vx = 0;
			}
			if(fabs(marker.pose.pose.position.y) < tolerance)
			{
				vz = 0;
			}*/
			/*if(flag == 0)
			{
				break;
			}
		
			drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
								Flight::VerticalLogic::VERTICAL_VELOCITY |
								Flight::YawLogic::YAW_ANGLE |
								Flight::HorizontalCoordinate::HORIZONTAL_BODY |
								Flight::SmoothMode::SMOOTH_ENABLE,
								vx, 0, 0, 0 );
						usleep(20000);
		
			ros::spinOnce();
		}
	}
	
}


int main(int argc, char** argv)
{

  ros::init(argc, argv, "follow_path");
  DJIPathFollower p;

  while(ros::ok())
    {
		p.track_ar_marker();
		
		ros::spinOnce();
    }
  
  


  return 0;
}*/

#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <ar_track_alvar_msgs/AlvarMarker.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <guidance/OffsetData.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/LaserScan.h>//for obstale distance

using namespace DJI::onboardSDK;
#define AS_PI 3.141590118

ros::Subscriber sub1;
DJIDrone* drone;

double rad2deg(double angle)
{
  return (angle*180)/AS_PI;
}

double quat2yaw(double q0, double q1, double q2, double q3)
{
  return rad2deg(AS_PI - atan2(2*(q0*q3 + q1*q2), -1 + 2*(q2*q2 + q3*q3)));
}

class DJIPathFollower
{
public:
  ros::Subscriber sub1, sub2, sub3;
  ros::NodeHandle nh;
  DJIDrone* drone;
  guidance::OffsetData data;
  sensor_msgs::LaserScan data_ul;
  ar_track_alvar_msgs::AlvarMarker marker;
  int flag;
  double myYaw;
  char key;
  double lower_b = 1.57, upper_b = 1.6, gain = 0.6;
  double lower_b_y = -0.1, upper_b_y = 0.1;
   
public:
  DJIPathFollower(double l_b, double u_b, double gain_i, double l_b_y, double u_b_y);
  ~DJIPathFollower();
  void offsetData_callback(const guidance::OffsetData& msg);
  void arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg);
  void obstacle_dist_callback(const sensor_msgs::LaserScan& msg);
  void track_ar_marker(void);
  double get_currentyaw(void);
  void track_ar_yaw(void);

};

DJIPathFollower::DJIPathFollower(double l_b, double u_b, double gain_i, double l_b_y, double u_b_y)
{
  drone = new DJIDrone(nh);
  sub1 = nh.subscribe("/guidance/posoffset", 10, &DJIPathFollower::offsetData_callback, this);
  sub2 = nh.subscribe("/ar_pose_marker", 10, &DJIPathFollower::arPose_callback, this);
  sub3 = nh.subscribe("/guidance/ultrasonic", 10, &DJIPathFollower::obstacle_dist_callback, this);
  key = 'a';
  flag = 0;
  lower_b = l_b, upper_b = u_b, gain = gain_i;
  lower_b_y = l_b_y, upper_b_y = u_b_y;
  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}

DJIPathFollower::~DJIPathFollower()
{
  delete drone;
}
void DJIPathFollower::offsetData_callback(const guidance::OffsetData& msg)
{
  this->data = msg;
}

void DJIPathFollower::obstacle_dist_callback(const sensor_msgs::LaserScan& msg)
{
 this->data_ul = msg;
} 
void DJIPathFollower::arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg)
{   
	
	if(msg.markers.size() > 0)
	{
		this->marker = msg.markers[0];
		flag = 1;
	}else
	{
		flag = 0;
	}
		
}

double DJIPathFollower::get_currentyaw(void)
{
	return quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}
void DJIPathFollower::track_ar_yaw(void)
{
}

void DJIPathFollower::track_ar_marker(void)
{
	double x_pos = marker.pose.pose.position.x;
	double y_pos = marker.pose.pose.position.y;
	double z_pos = marker.pose.pose.position.z;
	double vy = 0.1, vz = 0.1, vx = 0.1;
	double mean = (lower_b + upper_b)/2;
	
	double tolerance = 0.5;
	if( flag == 1)
	{
		while(ros::ok())
		{
			ROS_INFO("x: %f y: %f z: %f", marker.pose.pose.position.x, marker.pose.pose.position.y, marker.pose.pose.position.z);
			if(marker.pose.pose.position.z >= lower_b)
			{
				vx = gain*(marker.pose.pose.position.z - mean);

				if(vx >= 0.4)
				{
					vx = 0.3;
				}				

			}else if(marker.pose.pose.position.z <= upper_b)
			{
				vx = -gain*(mean - marker.pose.pose.position.z);

				if(vx <= -0.4)
				{
					vx = -0.3;
				}
	
				
			}else
			{
			 	vx = 0;
			}
			
			if(marker.pose.pose.position.y < lower_b_y)
			{
				vz = 0.1;
				
			}else if(marker.pose.pose.position.y > upper_b_y)
			{
				vz = -0.1;
			}else
			{
				vz = 0;
			}
			
			if(data_ul.intensities[0] == 1)
			{
				printf("distance: %lf\n",data_ul.ranges[0]);
			}
			/*if(fabs(marker.pose.pose.position.x) < tolerance)
			{
				vy = 0;
				vx = 0;
			}
			if(fabs(marker.pose.pose.position.y) < tolerance)
			{
				vz = 0;
			}*/
			if(flag == 0)
			{
				break;
			}
		
			drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
								Flight::VerticalLogic::VERTICAL_VELOCITY |
								Flight::YawLogic::YAW_ANGLE |
								Flight::HorizontalCoordinate::HORIZONTAL_BODY |
								Flight::SmoothMode::SMOOTH_ENABLE,
								vx, 0, vz, myYaw );
						usleep(20000);
		
			ros::spinOnce();
		}
	}
	
}


int main(int argc, char** argv)
{

  ros::init(argc, argv, "follow_path");

  double x = 0, y = 0, z = 0, a = 0, b = 0; //target location
  if(argc < 5)
    {
      return -1; 
    }else
    {
      x = atof(argv[1]); y = atof(argv[2]); z = atof(argv[3]);
      a = atof(argv[4]); b = atof(argv[5]);
      
    }

	DJIPathFollower p(x,y,z,a,b);

  while(ros::ok())
    {
		p.track_ar_marker();
		
		ros::spinOnce();
    }
  
  


  return 0;
}
