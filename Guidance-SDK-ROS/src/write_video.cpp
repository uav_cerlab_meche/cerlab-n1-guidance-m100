#include <stdio.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>

#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#define WIDTH 320
#define HEIGHT 240
#define IMAGE_SIZE (HEIGHT * WIDTH)


using namespace cv;
using namespace std;

Size frameSize(WIDTH, HEIGHT);
VideoCapture oVideo("~/home/cerlab/hough.avi", CV_FOURCC('8', 'B', 'P', 'S'), 30, frameSize);

void imageReceived_right(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_im;
  
  try
    {
      cv_im = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
    }
  catch(cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
  
  Mat src = cv_im->image;
}
