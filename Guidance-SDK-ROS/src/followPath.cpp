#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <ar_track_alvar_msgs/AlvarMarker.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <guidance/OffsetData.h>
#include <geometry_msgs/TransformStamped.h>

using namespace DJI::onboardSDK;
#define AS_PI 3.141590118

ros::Subscriber sub1;
DJIDrone* drone;

double rad2deg(double angle)
{
  return (angle*180)/AS_PI;
}

double quat2yaw(double q0, double q1, double q2, double q3)
{
  return rad2deg(AS_PI - atan2(2*(q0*q3 + q1*q2), -1 + 2*(q2*q2 + q3*q3)));
}

class DJIPathFollower
{
public:
  ros::Subscriber sub1, sub2;
  ros::NodeHandle nh;
  DJIDrone* drone;
  guidance::OffsetData data;
  double myYaw;
  
public:
  DJIPathFollower();
  ~DJIPathFollower();
  void offsetData_callback(const guidance::OffsetData& msg);
  void printPose_callback(const ar_track_alvar_msgs::AlvarMarker::ConstPtr& msg);
  void control_height(void);
  void control_yaw(void);
  void move_forward(void);
};

DJIPathFollower::DJIPathFollower()
{
  drone = new DJIDrone(nh);
  sub1 = nh.subscribe("/guidance/posoffset", 10, &DJIPathFollower::offsetData_callback, this);
  sub2 = nh.subscribe("/ar_pose_marker", 10, &DJIPathFollower::printPose_callback, this);
  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}

DJIPathFollower::~DJIPathFollower()
{
  delete drone;
}
void DJIPathFollower::offsetData_callback(const guidance::OffsetData& msg)
{
  this->data = msg;
}

void DJIPathFollower::printPose_callback(const ar_track_alvar_msgs::AlvarMarker::ConstPtr& msg)
{   
    ROS_INFO("x: %f, y: %f, z: %f, q0: %f, q1: %f, q2: %f, q3: %f", msg->pose.pose.position.x, msg->pose.pose.position.y, msg->pose.pose.position.z, msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
}

void DJIPathFollower::control_height(void)
{
  if(data.reliability == 1)
    {
      double vx = 0.3, vy = 0.3, vz = 0.3;
 
  if(data.dx_l <= 0)
    {
      while(ros::ok())
	{
	  double current_yaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
	  
	  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
				   Flight::VerticalLogic::VERTICAL_VELOCITY|
				   Flight::YawLogic::YAW_ANGLE|
				   Flight::HorizontalCoordinate::HORIZONTAL_BODY |
				   Flight::SmoothMode::SMOOTH_ENABLE,
				   0, vy, 0, current_yaw);
	  usleep(20000);
	  ROS_INFO("Moving Right");
	  if((data.dx_l > 0 && data.dx_l < 25)||drone->local_position.z >= 5)
	    {
	      break;
	    }
	  ros::spinOnce();
	}
    }
  if(data.dx_l >= 25)
    {
      while(ros::ok())
	{
	  double current_yaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);

	  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
				   Flight::VerticalLogic::VERTICAL_VELOCITY|
				   Flight::YawLogic::YAW_ANGLE|
				   Flight::HorizontalCoordinate::HORIZONTAL_BODY |
				   Flight::SmoothMode::SMOOTH_ENABLE,
				   0, -vy, 0, current_yaw);
	  usleep(20000);
	  ROS_INFO("Moving left");
	  if((data.dx_l > 0 && data.dx_l < 25)||drone->local_position.z <= 2)
	    {
	      break;
	    }
	  ros::spinOnce();
	}
      	  
    }

    }
  
}

void DJIPathFollower::control_yaw(void)
{
  if(!isnan(data.yaw)&&!isinf(data.yaw))
    {
      double y_rate = 2;
  if(fabs(rad2deg(data.yaw)) > 5)
    {
       double final_angle = data.yaw + quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3); 
       if(final_angle > 360)
	 {
	   final_angle = final_angle - 360;
	 }else if(final_angle < 0)
	 {
	   final_angle = 360 + final_angle;
	 }

      if(rad2deg(data.yaw) > 0)
	{
	  while(ros::ok())
	    {

	      double Angle = quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);

	      drone->attitude_control(0x4B, 0, 0, 0, y_rate);
	      //ROS_INFO("Yawing Right");
	      usleep(20000);
	      ROS_INFO("yawr_data: %f,yawr_final: %f, c_angle: %f",(rad2deg(data.yaw)), final_angle, Angle);
	      if(fabs(final_angle - Angle) <= 0.1)//Angle > 1.1*final_angle || Angle < 0.9*final_angle)
		{
		  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
		  break;
		}

	      ros::spinOnce();
	    }
	}else
	{

	   while(ros::ok())
	    {
	      double Angle =quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
				      
	      drone->attitude_control(0x4B, 0, 0, 0, -y_rate);
	      //ROS_INFO("Yawing Right");
	      usleep(20000);
	      ROS_INFO("yawl_data: %f,yawl_final: %f, c_angle: %f",(rad2deg(data.yaw)), final_angle, Angle);
	      if(fabs(final_angle - Angle) <= 0.1)//Angle > 1.1*final_angle || Angle < 0.9*final_angle)
		{
		  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
		  break;
		}

	      ros::spinOnce();
	    }
	  
	}
      
    }

    }
  
}

void DJIPathFollower::move_forward(void)
{
  if(data.reliability == 1)
    {
       for( int i = 0; i < 300; i++)
    {
      double current_yaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
      drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY|
			       Flight::VerticalLogic::VERTICAL_VELOCITY|
			       Flight::YawLogic::YAW_ANGLE|
			       Flight::HorizontalCoordinate::HORIZONTAL_BODY|
			       Flight::SmoothMode::SMOOTH_ENABLE,
			       0.4, 0, 0, current_yaw);
      ROS_INFO("Moving Ahead yaw: %f", myYaw);
      
	usleep(20000);
    }
    }
 
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "follow_path");
  DJIPathFollower p;

  // double initial_yaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
  // p.myYaw=initial_yaw;
  while(ros::ok())
    {
      // ROS_INFO("msg.dx_l: %f, msg.dx_r: %f, msg.slope_l: %f, msg.slope_r: %f\n", p.data.dx_l, p.data.dx_r, p.data.slope_l, p.data.slope_r);
      // if(p.data.reliability == 1)
	{
	  //p.control_height();
	  //p.control_yaw();
	  p.move_forward();
	}
      

      ros::spinOnce();
    }
  
  


  return 0;
}
