#include <stdio.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <math.h>
#include <vector>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <dji_sdk/dji_drone.h>

#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <guidance/OffsetData.h>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>


using namespace cv;
using namespace std;

int edgeThresh = 1;
int lowThreshold = 60;
int kernel_size = 3;
int key = 0;

#define WIDTH 320
#define HEIGHT 240
#define IMAGE_SIZE (HEIGHT * WIDTH)
#define AS_PI 3.141590118

int num = 0;

float patch_size = 5;
float int_thresh_dynamic[6]={0.9,0.5,0.3,0.2,0.1,0.05};
float fraction_path_thresh = 0.1;


cv::Mat find_path_blob_dynamic(Mat I)
{
	float fraction_path = 0.0;
	int intensity_index = 0;
	float thresh_tex = 0.1;
	
//Defining the edge filters and parameters for filter2D function	
	  Mat Gx,Gy,G_45;
	  Gx =Mat::zeros(3,3, CV_32F);
	  Gy =Mat::zeros(3,3, CV_32F);
	  G_45 =Mat::zeros(3,3, CV_32F);

	  Gx.at<float>(0,0)=-1;
	  Gx.at<float>(0,2)=1;
	  Gx.at<float>(1,0)=-1;
	  Gx.at<float>(1,2)=1;
	  Gx.at<float>(2,0)=-1;
	  Gx.at<float>(2,2)=1;

	  Gy.at<float>(0,0)=-1;
	  Gy.at<float>(0,1)=-1;
	  Gy.at<float>(0,2)=-1;
	  Gy.at<float>(2,0)=1;
	  Gy.at<float>(2,1)=1;
	  Gy.at<float>(2,2)=1;
	 
	  G_45.at<float>(0,1)=1;
	  G_45.at<float>(1,0)=1;
	  G_45.at<float>(1,2)=-1;
	  G_45.at<float>(2,1)=-1;
	  
	  
	  Mat I_new_Gx;
	  Mat I_new_Gy;
	  Mat I_new_G_45;
	  Point anchor=Point(-1,-1);
	  int ddepth = -1;
	  double delta=0;
	  int borderType=BORDER_DEFAULT;

//Defining dilating and eroding parameters
	Mat dil_erode_output;
	Mat element_dil=getStructuringElement(MORPH_ELLIPSE,Size(10,10),Point(1,1));	
	Mat element_erode =getStructuringElement(MORPH_ELLIPSE,Size(5,5),Point(1,1));
	
	//Gabor filter parameters
      Mat gab_kernel;
	  
	  int ksize = 11;
	  double sigma_g = 1.0;
	  double theta_g = CV_PI/4;
	  double lambd_g = 15.0;
	  double gamma_g = 0.02;
	  double psi_g = CV_PI*0.5;
	  int ktype = CV_32F;
	  gab_kernel = getGaborKernel(Size(ksize,ksize),sigma_g,theta_g,lambd_g,gamma_g,psi_g,ktype);
	
	Mat Gab_response;
	filter2D(I,Gab_response,ddepth,gab_kernel,anchor,delta,borderType);
	//imshow("Gabor filter response",Gab_response);
	
	
	
	//Dynamic intensity thresholding
	while((fraction_path < fraction_path_thresh) && (sizeof(int_thresh_dynamic)/sizeof(int_thresh_dynamic[0])>=intensity_index))
	{
	    
	  float thresh_int = int_thresh_dynamic[intensity_index];
	  
	  
	  filter2D(I,I_new_Gx,ddepth,Gx,anchor,delta,borderType);
	  //imshow("Gx_filtered_image",I_new_Gx);
	  

	  filter2D(I,I_new_Gy,ddepth,Gy,anchor,delta,borderType);
	  //imshow("Gy_filtered_image",I_new_Gy);
	  //cv::waitKey(0);

	  filter2D(I,I_new_G_45,ddepth,G_45,anchor,delta,borderType);
	  //imshow("G_45_filtered_image",I_new_G_45);
	  //cv::waitKey(0);
	  
	  Mat I_new_da = I_new_Gx+I_new_Gy +I_new_G_45;
	  //imshow("Dir_am_filtered_image",I_new_da);
	  
	  
	  double min, max;
	  
	  minMaxIdx(I_new_da,&min,&max);
	  Mat adjMap;
	  float scale = 255/(max-min);
	  I_new_da.convertTo(adjMap,CV_8UC1,scale,-min*scale);
	  Mat resultMap;
	  applyColorMap(adjMap,resultMap,COLORMAP_OCEAN);
	  
	  //imshow("Dir_am_visual",resultMap);
	  Mat I_new_0_1= I_new_da; //Normalizing the image
	  
	  
	  
	  
	  //minMaxIdx(I_new_0_1, &min, &max);
	  //cout<<min<<endl;
	  //cout<<max<<endl;

	  //minMaxIdx(I, &min, &max);
	  //cout<<min<<endl;
	  //cout<<max<<endl;


	  float num_pix_patch = patch_size*patch_size;
	  Mat temp_image_patch,temp_tex_patch, output_binary_image;
	  
	  temp_image_patch = Mat::zeros(int(patch_size),int(patch_size), CV_32F);
	  temp_tex_patch = Mat::zeros(int(patch_size),int(patch_size), CV_32F);
	  
	  output_binary_image = Mat::zeros(I_new_0_1.rows,I_new_0_1.cols,CV_32F); 
	  
	  int row_index_max = I_new_0_1.rows/patch_size;
	  int col_index_max = I_new_0_1.cols/patch_size;
	  //printf("\n%d %d",row_index_max,col_index_max);
	  
	  
	  //Adaptive image thresholding
	  //Mat I_thresh = image;
	  //GaussianBlur(image,image,Size(5,5),0,0,BORDER_DEFAULT);
	  //adaptiveThreshold(image,I_thresh,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,9,2);
	  
	  
	  //imshow("Theshold_image",I_thresh);
	  //waitKey(0);
	  

	  for(int i=0;i<row_index_max;i++)
		{
		  for(int j=0;j<col_index_max;j++)
		{
		  //printf("\nTest00 i=%d j=%d",i,j);
		  Rect patch_index(j*patch_size,i*patch_size,patch_size,patch_size);
		  //printf("\nTest01");
		  temp_tex_patch = I_new_0_1(patch_index);
		  //printf("\nTest02");
		  temp_image_patch = I(patch_index);
		  //printf("\nTest03");
		  //printf("\n%f %f",sum(temp_tex_patch)[0]/(patch_size*patch_size),sum(temp_image_patch)[0]/(patch_size*patch_size));
		  if (((sum(temp_tex_patch)[0]/num_pix_patch)<thresh_tex) && ((sum(temp_image_patch)[0]/num_pix_patch)>thresh_int))
			{
			  //printf("\nYes");
			  output_binary_image(patch_index)=1;
			}
		  //	 	      output_binary_image(patch_index)=0.0;
		}
		}

	Rect upper_half(0,0,float(I_new_0_1.cols),float(I_new_0_1.rows/2));
	output_binary_image(upper_half)=0.0;
	//printf("\n%f",sum(output_binary_image)[0]/(output_binary_image.rows*output_binary_image.cols));
  
  
	
	dilate(output_binary_image,dil_erode_output,element_dil);
	
	erode(dil_erode_output,dil_erode_output,element_dil);
	//imshow("binary_output",output_binary_image);
	
	fraction_path = (sum(dil_erode_output)[0]*2.0)/(dil_erode_output.rows*dil_erode_output.cols);
	//printf("fraction of path=%f",fraction_path);
	//waitKey(0);
	intensity_index++;
	}
	
	//Display the final image
	//imshow("dilated_eroded_binary_output",dil_erode_output);
	
	return dil_erode_output;
	
    
}

ros::Publisher offset_data_pub;

void imageReceived_right(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_im;
  std::vector<int> compression_param, binary_param;
  compression_param.push_back(CV_IMWRITE_PNG_COMPRESSION);
  compression_param.push_back(3);
  binary_param.push_back(CV_IMWRITE_PXM_BINARY);
  binary_param.push_back(1);
  
	  
  try
    {
      cv_im = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
      
	  
    }
  catch(cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
  
  Mat src = cv_im->image;
  Mat src_p = cv_im->image;
  imshow("right_image", src);
  key = waitKey(1);
    
  src_p.convertTo(src_p, CV_32F); 
  src_p=src_p/255.0;
  Mat dst2 = find_path_blob_dynamic(src_p);
 
  
  imshow("dilated_eroded_binary_output",dst2);
  //cout<<bin<<endl;
  
  Mat detected_edges, dst;
  blur(src, detected_edges,Size(3,3));
  int ratio = 3;

  Canny(detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size);

  dst = Scalar::all(0);
  src.copyTo(dst, detected_edges);
  std::vector<Vec2f> lines;

  for(int i = 0; i < (int)(dst.rows/2); i++)
    {
      for(int j = 0; j < dst.cols; j++)
	{
	  detected_edges.at<double>(i,j) = 0;
	}
    }


  HoughLines(detected_edges, lines, 1, CV_PI/180, 50);
 
  double mean_ag1 = 0, mean_ag2 = 0;
  Point2f pt11, pt12, pt21, pt22;
  int flag1 = 0, flag2 = 0;

  for(size_t i=0; i<lines.size(); i++)
    {
      float rho = lines[i][0], theta = lines[i][1];
      Point2f pt1, pt2;
      double a = cos(theta), b = sin(theta);
      double x0 = a*rho, y0 = b*rho;
      pt1.x = cvRound(x0 + 1000*(-b));
      pt1.y = cvRound(y0 + 1000*(a));
      pt2.x = cvRound(x0 - 1000*(-b));
      pt2.y = cvRound(y0 - 1000*(a));
      double thresag = atan2((pt1.y-pt2.y),(pt1.x-pt2.x));
      // line(dst, pt1, pt2, Scalar(255,0,0), 1, CV_AA);
      if(fabs(thresag) > 10*(CV_PI/180) && fabs(thresag) < 160*(CV_PI/180))
	{
	  // line(dst, pt1, pt2, Scalar(255,0,0), 1, CV_AA);
	   if(flag1 == 0 && thresag > 0)
	    {
	      mean_ag1 = thresag;
	      pt11.x = pt1.x;
	      pt11.y = pt1.y;
	      pt12.x = pt2.x;
	      pt12.y = pt2.y;
	      flag1 = 1;

	    }else if(thresag > 0 && fabs(thresag) < 1.1*fabs(mean_ag1) && fabs(thresag) > 0.8*fabs(mean_ag1) && flag1 == 1)
	    {
	      mean_ag1 = (mean_ag1 + thresag)/2;
	      pt11.x = (pt11.x + pt1.x)/2;
	      pt11.y = (pt11.y + pt1.y)/2;
	      pt12.x = (pt12.x + pt2.x)/2;
	      pt12.y = (pt12.y + pt2.y)/2;
	    }

	   if( flag2 == 0 && thresag < 0)
	    {
	      mean_ag2 = thresag;
	      pt21.x = pt1.x;
	      pt21.y = pt1.y;
	      pt22.x = pt2.x;
	      pt22.y = pt2.y;
	      flag2 = 1;

	    }else if(thresag < 0 && fabs(thresag) < 1.1*fabs(mean_ag2) && fabs(thresag) > 0.8*fabs(mean_ag2) && flag2 == 1)
	    {
	      mean_ag2 = (mean_ag2 + thresag)/2;
	      pt21.x = (pt21.x + pt1.x)/2;
	      pt21.y = (pt21.y + pt1.y)/2;
	      pt22.x = (pt22.x + pt2.x)/2;
	      pt22.y = (pt22.y + pt2.y)/2;
	    }
	}
    }

   line(dst, pt11, pt12, Scalar(255,0,0), 1, CV_AA);
   line(dst, pt21, pt22, Scalar(255,0,0), 1, CV_AA);
  imshow("subscribed image_right", dst);
   key = waitKey(1);
   
   if((char)key == 'c')
	{
	  const string str1 =(( "images/main/test"  + to_string(num)) + ".png");
	  const string str2 =(( "images/hough/test"  + to_string(num)) + ".png");
	  const string str3 =(( "images/blob/test"  + to_string(num)) + ".ppm");
	  imwrite(str1, cv_im->image, compression_param);
	  imwrite(str2, dst, compression_param);
	  imwrite(str3, dst2, binary_param);
	  num++;
	}

  double x_l = 0, x_r = 0;
  
  x_l = (((double)src.rows - pt11.y)/tan(mean_ag1)) + pt11.x;
  x_r = (((double)src.rows - pt21.y)/tan(mean_ag2)) + pt21.x;

  double c1 = 0, c2 = 0;
  c1 = pt11.y - tan(mean_ag1)*pt11.x;
  c2 = pt21.y - tan(mean_ag2)*pt21.x;

  Point2f ptct1, ptct2;
  ptct1.x = (c1 - c2)/(tan(mean_ag2) - tan(mean_ag1));
  ptct1.y = tan(mean_ag1)*ptct1.x + c1;

  ptct2.x = (x_r + x_l)/2;
  ptct2.y = 240;
  guidance::OffsetData p;
  p.header.frame_id = "guidance";
  p.header.stamp = ros::Time::now();
  p.slope_l = mean_ag1;
  p.slope_r = mean_ag2;
  p.dx_l = x_l;
  p.dx_r = x_r;
  p.c_point_x = ptct1.x;
  p.c_point_y = ptct1.y;
  p.yaw = -(( AS_PI/2) - (atan2((ptct2.y - ptct1.y),(ptct2.x - ptct1.x)))); 
  if(isnan(p.yaw) || isinf(p.yaw) || p.c_point_y == 0)
    {
      p.reliability = 0;

    }else
    {
      p.reliability = 1;
    }
  offset_data_pub.publish(p);

  key =  waitKey(3);  
 }


int main(int argc, char **argv)
{
  
  ros::init(argc,argv,"improcNode");
  ros::NodeHandle nh;
  offset_data_pub = nh.advertise<guidance::OffsetData>("/guidance/posoffset", 1);
    {
       if(key == 'q')
	{
	  return 0;
	}
       printf("key : %c", key);
       // ros::Subscriber sub1 = nh.subscribe("/guidance/left_image",1,&imageReceived_left);
  
	  ros::Subscriber sub2 = nh.subscribe("/guidance/right_image",1,&imageReceived_right);

  ros::spin();
    }
  

  return 0;
}
