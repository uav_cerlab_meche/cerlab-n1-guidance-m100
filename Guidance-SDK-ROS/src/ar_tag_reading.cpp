#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <ar_track_alvar_msgs/AlvarMarker.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <guidance/OffsetData.h>
#include <geometry_msgs/TransformStamped.h>

using namespace DJI::onboardSDK;
#define AS_PI 3.141590118

ros::Subscriber sub1;
DJIDrone* drone;

double rad2deg(double angle)
{
  return (angle*180)/AS_PI;
}

double quat2yaw(double q0, double q1, double q2, double q3)
{
  return rad2deg(AS_PI - atan2(2*(q0*q3 + q1*q2), -1 + 2*(q2*q2 + q3*q3)));
}

class DJIPathFollower
{
public:
  ros::Subscriber sub1, sub2;
  ros::NodeHandle nh;
  DJIDrone* drone;
  guidance::OffsetData data;
  ar_track_alvar_msgs::AlvarMarker marker;
  int flag;
  double myYaw;
  char key;

   
public:
  DJIPathFollower();
  ~DJIPathFollower();
  void offsetData_callback(const guidance::OffsetData& msg);
  void arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg);
  void ar_tag_read(void);

};

DJIPathFollower::DJIPathFollower()
{
  drone = new DJIDrone(nh);
  sub1 = nh.subscribe("/guidance/posoffset", 10, &DJIPathFollower::offsetData_callback, this);
  sub2 = nh.subscribe("/ar_pose_marker", 10, &DJIPathFollower::arPose_callback, this);
  key = 'a';
  flag = 0;
  myYaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
}

DJIPathFollower::~DJIPathFollower()
{
  delete drone;
}
void DJIPathFollower::offsetData_callback(const guidance::OffsetData& msg)
{
  this->data = msg;
}

void DJIPathFollower::arPose_callback(const ar_track_alvar_msgs::AlvarMarkers& msg)
{   
	
	if(msg.markers.size() > 0)
	{
		this->marker = msg.markers[0];
		flag = 1;
	}else
	{
		flag = 0;
	}
		
}

void DJIPathFollower::ar_tag_read(void)
{
	double x_pos = marker.pose.pose.position.x;
	double y_pos = marker.pose.pose.position.y;
	double z_pos = marker.pose.pose.position.z;
	double vy = 0.1, vz = 0.1, vx = 0.1;
	double tolerance = 0.09;
	
	ROS_INFO("x: %f y: %f z: %f", marker.pose.pose.position.x, marker.pose.pose.position.y, marker.pose.pose.position.z);
		
}


int main(int argc, char** argv)
{

  ros::init(argc, argv, "follow_path");
  DJIPathFollower p;

  while(ros::ok())
    {
		p.ar_tag_read();
		
		ros::spinOnce();
    }
  
  


  return 0;
}
