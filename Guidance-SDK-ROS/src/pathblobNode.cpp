#include <stdio.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <dji_sdk/dji_drone.h>

#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <guidance/OffsetData.h>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>



using namespace cv;
using namespace std;

char key = 0;
float patch_size = 5;
float int_thresh_dynamic[6]={0.9,0.5,0.3,0.2,0.1,0.05};
float fraction_path_thresh = 0.1;


void find_path_blob_dynamic(Mat I)
{
	float fraction_path = 0.0;
	int intensity_index = 0;
	float thresh_tex = 0.1;
	
//Defining the edge filters and parameters for filter2D function	
	  Mat Gx,Gy,G_45;
	  Gx =Mat::zeros(3,3, CV_32F);
	  Gy =Mat::zeros(3,3, CV_32F);
	  G_45 =Mat::zeros(3,3, CV_32F);

	  Gx.at<float>(0,0)=-1;
	  Gx.at<float>(0,2)=1;
	  Gx.at<float>(1,0)=-1;
	  Gx.at<float>(1,2)=1;
	  Gx.at<float>(2,0)=-1;
	  Gx.at<float>(2,2)=1;

	  Gy.at<float>(0,0)=-1;
	  Gy.at<float>(0,1)=-1;
	  Gy.at<float>(0,2)=-1;
	  Gy.at<float>(2,0)=1;
	  Gy.at<float>(2,1)=1;
	  Gy.at<float>(2,2)=1;
	 
	  G_45.at<float>(0,1)=1;
	  G_45.at<float>(1,0)=1;
	  G_45.at<float>(1,2)=-1;
	  G_45.at<float>(2,1)=-1;
	  
	  
	  Mat I_new_Gx;
	  Mat I_new_Gy;
	  Mat I_new_G_45;
	  Point anchor=Point(-1,-1);
	  int ddepth = -1;
	  double delta=0;
	  int borderType=BORDER_DEFAULT;

//Defining dilating and eroding parameters
	Mat dil_erode_output;
	Mat element_dil=getStructuringElement(MORPH_ELLIPSE,Size(10,10),Point(1,1));	
	Mat element_erode =getStructuringElement(MORPH_ELLIPSE,Size(5,5),Point(1,1));
	
	//Gabor filter parameters
      Mat gab_kernel;
	  
	  int ksize = 11;
	  double sigma_g = 1.0;
	  double theta_g = CV_PI/4;
	  double lambd_g = 15.0;
	  double gamma_g = 0.02;
	  double psi_g = CV_PI*0.5;
	  int ktype = CV_32F;
	  gab_kernel = getGaborKernel(Size(ksize,ksize),sigma_g,theta_g,lambd_g,gamma_g,psi_g,ktype);
	
	Mat Gab_response;
	filter2D(I,Gab_response,ddepth,gab_kernel,anchor,delta,borderType);
	imshow("Gabor filter response",Gab_response);
	
	
	
	//Dynamic intensity thresholding
	while((fraction_path < fraction_path_thresh) && (sizeof(int_thresh_dynamic)/sizeof(int_thresh_dynamic[0])>=intensity_index))
	{
	    
	  float thresh_int = int_thresh_dynamic[intensity_index];
	  
	  
	  filter2D(I,I_new_Gx,ddepth,Gx,anchor,delta,borderType);
	  //imshow("Gx_filtered_image",I_new_Gx);
	  

	  filter2D(I,I_new_Gy,ddepth,Gy,anchor,delta,borderType);
	  //imshow("Gy_filtered_image",I_new_Gy);
	  //cv::waitKey(0);

	  filter2D(I,I_new_G_45,ddepth,G_45,anchor,delta,borderType);
	  //imshow("G_45_filtered_image",I_new_G_45);
	  //cv::waitKey(0);
	  
	  Mat I_new_da = I_new_Gx+I_new_Gy +I_new_G_45;
	  imshow("Dir_am_filtered_image",I_new_da);
	  
	  
	  double min, max;
	  
	  minMaxIdx(I_new_da,&min,&max);
	  Mat adjMap;
	  float scale = 255/(max-min);
	  I_new_da.convertTo(adjMap,CV_8UC1,scale,-min*scale);
	  Mat resultMap;
	  applyColorMap(adjMap,resultMap,COLORMAP_OCEAN);
	  
	  imshow("Dir_am_visual",resultMap);
	  Mat I_new_0_1= I_new_da; //Normalizing the image
	  
	  
	  
	  
	  //minMaxIdx(I_new_0_1, &min, &max);
	  //cout<<min<<endl;
	  //cout<<max<<endl;

	  //minMaxIdx(I, &min, &max);
	  //cout<<min<<endl;
	  //cout<<max<<endl;


	  float num_pix_patch = patch_size*patch_size;
	  Mat temp_image_patch,temp_tex_patch, output_binary_image;
	  
	  temp_image_patch = Mat::zeros(int(patch_size),int(patch_size), CV_32F);
	  temp_tex_patch = Mat::zeros(int(patch_size),int(patch_size), CV_32F);
	  
	  output_binary_image = Mat::zeros(I_new_0_1.rows,I_new_0_1.cols,CV_32F); 
	  
	  int row_index_max = I_new_0_1.rows/patch_size;
	  int col_index_max = I_new_0_1.cols/patch_size;
	  //printf("\n%d %d",row_index_max,col_index_max);
	  
	  
	  //Adaptive image thresholding
	  //Mat I_thresh = image;
	  //GaussianBlur(image,image,Size(5,5),0,0,BORDER_DEFAULT);
	  //adaptiveThreshold(image,I_thresh,255,ADAPTIVE_THRESH_GAUSSIAN_C,THRESH_BINARY,9,2);
	  
	  
	  //imshow("Theshold_image",I_thresh);
	  //waitKey(0);
	  

	  for(int i=0;i<row_index_max;i++)
		{
		  for(int j=0;j<col_index_max;j++)
		{
		  //printf("\nTest00 i=%d j=%d",i,j);
		  Rect patch_index(j*patch_size,i*patch_size,patch_size,patch_size);
		  //printf("\nTest01");
		  temp_tex_patch = I_new_0_1(patch_index);
		  //printf("\nTest02");
		  temp_image_patch = I(patch_index);
		  //printf("\nTest03");
		  //printf("\n%f %f",sum(temp_tex_patch)[0]/(patch_size*patch_size),sum(temp_image_patch)[0]/(patch_size*patch_size));
		  if (((sum(temp_tex_patch)[0]/num_pix_patch)<thresh_tex) && ((sum(temp_image_patch)[0]/num_pix_patch)>thresh_int))
			{
			  //printf("\nYes");
			  output_binary_image(patch_index)=1;
			}
		  //	 	      output_binary_image(patch_index)=0.0;
		}
		}

	Rect upper_half(0,0,float(I_new_0_1.cols),float(I_new_0_1.rows/2));
	output_binary_image(upper_half)=0.0;
	//printf("\n%f",sum(output_binary_image)[0]/(output_binary_image.rows*output_binary_image.cols));
  
  
	
	dilate(output_binary_image,dil_erode_output,element_dil);
	
	erode(dil_erode_output,dil_erode_output,element_dil);
	//imshow("binary_output",output_binary_image);
	
	fraction_path = (sum(dil_erode_output)[0]*2.0)/(dil_erode_output.rows*dil_erode_output.cols);
	//printf("fraction of path=%f",fraction_path);
	//waitKey(0);
	intensity_index++;
	}
	
	//Display the final image
	imshow("dilated_eroded_binary_output",dil_erode_output);
	
    
}

void imageReceived_right(const sensor_msgs::ImageConstPtr& msg)
{
	cv_bridge::CvImagePtr cv_im;

	try
	{
		cv_im = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
	}
	catch(cv_bridge::Exception& e)
	{
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	Mat src = cv_im->image;
	imshow("right_image", src);
		  
	src.convertTo(src, CV_32F); 
	src=src/256.0;
		  
	find_path_blob_dynamic(src);

	key =  waitKey(3);  
 }


int main(int argc, char **argv)
{
  
  ros::init(argc,argv,"pathblobNode");
  ros::NodeHandle nh;
  //offset_data_pub = nh.advertise<guidance::OffsetData>("/guidance/posoffset", 1);
    {
       if(key == 'q')
	{
	  return 0;
	}
  printf("key : %c", key);
   // ros::Subscriber sub1 = nh.subscribe("/guidance/left_image",1,&imageReceived_left);

  ros::Subscriber sub2 = nh.subscribe("/guidance/right_image",1,&imageReceived_right);

  ros::spin();
    }
  
  return 0;
}
