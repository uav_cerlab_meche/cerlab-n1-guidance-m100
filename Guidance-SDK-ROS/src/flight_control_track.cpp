#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <stdlib.h>
#include <opencv2/highgui/highgui.hpp>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

using namespace DJI::onboardSDK;
using namespace cv;

#define AS_PI 3.14159

double rad2deg(double angle)
{
  return (angle*180)/AS_PI;
}

double quat2yaw(double q0, double q1, double q2, double q3)
{
  return rad2deg(AS_PI - atan2(2*(q0*q3 + q1*q2), -1 + 2*(q2*q2 + q3*q3)));
}

class PathFollower
{
public :
  ros::NodeHandle nh;
  DJIDrone* drone;
  

public :

  PathFollower();
  ~PathFollower(void);
  void takeoff(void);
  void landing(void);
  void build_reduce_height(const double h);
  void gohome(void);
  void obtain_control(void);
  void release_control(void);
  dji_sdk::LocalPosition getCurrentPosition(void);
  void generate_bezier_points(std::vector<Point3f> &x, double u, Point3f &p);
  void track_bezier_curve(std::vector<Point3f> &x);
  void stablize_flight(const dji_sdk::LocalPosition pos);
  void navigate_to(const double x, const double y, const double z, const double yaw);
  void test_yaw(double yaw);

};

PathFollower::PathFollower()
{
  drone = new DJIDrone(nh);
}

PathFollower::~PathFollower()
{
  delete drone;
}

void PathFollower::obtain_control()
{
   drone->request_sdk_permission_control();
}

void PathFollower::release_control()
{
   drone->release_sdk_permission_control();
}

void PathFollower::takeoff()
{
   drone->takeoff();
}

void PathFollower::build_reduce_height(const double h)
{
 
  double dz = 0.0;
  double vz = 2.0, vzz = 0.2;
  
  while(ros::ok())
                {
		  ROS_INFO("dz: %f",dz);
		  dz = h - drone->local_position.z;
		  if(dz < 0)
		    {
		      vz = -2.0; vzz = -0.2;
		    }else
		    {
		      vz = 2.0 ; vzz = 0.2;
		    }
		  if( fabs(dz) < 2)
		    {
		  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_POSITION |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
                            0, 0, vzz, 0 );
                    usleep(20000);
		    }else
		    {
		  drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_POSITION |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
                            0, 0, vz, 0 );
                    usleep(20000);
		    }

		    if(fabs(dz) < 0.1)
		      {
			break;
		      }
		    ros::spinOnce();
		    }
}

void PathFollower::landing()
{
   drone->landing();
}

void PathFollower::gohome()
{
   drone->gohome();
}

dji_sdk::LocalPosition PathFollower::getCurrentPosition()
{
  dji_sdk::LocalPosition pos;
  pos.ts = drone->local_position.ts;
  pos.x = drone->local_position.x;
  pos.y = drone->local_position.y;
  pos.z = drone->local_position.z;

  return pos;
}

void PathFollower::navigate_to(const double x, const double y, const double z, const double yaw)
{
  double dx = 0.0;
  double dy = 0.0;
  double dz = 0.0;

  double vx = 1, vy = 1, vz = 1;
  double vxx = 0.2, vyy = 0.2, vzz = 0.2;

  while(ros::ok())
    {
      dx = x - drone->local_position.x;
      dy = y - drone->local_position.y;
      dz = z - drone->local_position.z;
      if(dx < 0)
	{
	  vx = -1; vxx = -0.4;
	}else
	{
	  vx = 1; vxx = 0.4;
	}
      if(dy < 0)
	{
	  vy = -1; vyy = -0.4;
	}else
	{
	  vy = 1; vyy = 0.4;
	}
      if(dz < 0)
	{
	  vz = -1; vzz = -0.4;
	}else
	{
	  vz = 1; vzz = 0.4;
	}
      if( fabs(dx) < 1.0 && fabs(dy) < 1.0 && fabs(dz) < 1.0)
	{
        drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
				 vxx, vyy, vzz, yaw );
      usleep(20000);
	}else
	{
	          drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
                            Flight::VerticalLogic::VERTICAL_VELOCITY |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
				 vx, vy, vz, yaw );
      usleep(20000);
	}

      ROS_INFO("dx: %f, dy: %f, dz: %f", dx, dy, dz);
      if(fabs(dx) < 0.2 && fabs(dy) < 0.2 && fabs(dz) < 0.2)
	{
	  break;
	}
      ros::spinOnce();

      }
  }

void PathFollower::stablize_flight(const dji_sdk::LocalPosition pos)
{								
  /*ROS_INFO("stabalizing the flight...");
  dji_sdk::LocalPosition current_pos = drone->local_position;

  double dx = current_pos.x - pos.x;
  double dy = current_pos.y - pos.y;
  double dz = current_pos.z - pos.z;

  while(fabs(dx) <= 0.1 && fabs(dy) <= 0.1 && fabs(dz) <= 0.1)
    {
      drone->attitude_control(0x90, pos.x, pos.y, pos.z, 0);
      usleep(20000);

      current_pos = drone->local_position;
      dx = current_pos.x - pos.x;
      dy = current_pos.y - pos.y;
      dz = current_pos.z - pos.z;
      }*/
}

void PathFollower::generate_bezier_points(std::vector<Point3f> &x, double u, Point3f &p)
{
  p = x[0]*pow(1-u,3) + x[1]*pow(1-u,2)*u*3 + x[2]*(1-u)*u*u*3 + x[3]*u*u*u;
 
}

void PathFollower::track_bezier_curve(std::vector<Point3f> &x)
{
  int count = 40;
  double u = 0;
  double step = 1.0/(double)count;
  Point3f p;
  double ang, V = 1.5;
  for(int i = 0; i <= count; i++)
    {
      generate_bezier_points(x,u,p);
      ang = atan2(p.y,p.x);
      ROS_INFO("vx: %f, vy: %f, ang: %f", V*cos((AS_PI/2)+ang), V*sin((AS_PI/2)+ang), ang);
      // ROS_INFO("x: %f, y: %f, z: %f", p.x, p.y, p.z);
       for(int j = 0; j < 10; j++)
	{
       drone->attitude_control( Flight::HorizontalLogic::HORIZONTAL_VELOCITY |
                            Flight::VerticalLogic::VERTICAL_POSITION |
                            Flight::YawLogic::YAW_ANGLE |
                            Flight::HorizontalCoordinate::HORIZONTAL_BODY |
                            Flight::SmoothMode::SMOOTH_ENABLE,
				V*cos((AS_PI/2)+ang), V*sin((AS_PI/2)+ang), p.z, 0 );
			    usleep(20000);
	}
      // navigate_to(p.x, p.y, p.z, 0.0);
      u = u +step;
    }

}

void PathFollower::test_yaw(double yaw)
{
  double current_yaw = quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
  
  if(yaw > 360)
    {
      yaw = yaw-360;

    }else if(yaw < 0)
    {
      yaw = 360 - yaw;
    }
  while(ros::ok())
    {
      ROS_INFO("c_yaw: %f",current_yaw);
      //drone->attitude_control(0x4B,0,0,0,5);
      drone->attitude_control(0x93,0,0,0,yaw);
      usleep(20000);
      current_yaw =  quat2yaw(drone->attitude_quaternion.q0, drone->attitude_quaternion.q1, drone->attitude_quaternion.q2, drone->attitude_quaternion.q3);
    
      if(fabs((current_yaw) - yaw) <= 0.1)
	{
	  break;
	}
      ros::spinOnce();
    }
}
					
int main(int argc, char** argv)
{
  ros::init(argc, argv, "flight_controller");
 
  PathFollower pf;
  int temp32;
  int flag = 0;
  dji_sdk::LocalPosition pos;
  std::vector<Point3f> pts;
  Point3f p;
  p.x = 10; p.y = 10; p.z = 20;
  pts.push_back(p);
  p.x = -10; p.y = 10; p.z = 25;
  pts.push_back(p);
  p.x = -10; p.y = -10; p.z = 20;
  pts.push_back(p);
  p.x = 10; p.y = -10; p.z = 25;
  pts.push_back(p);

  double x = 0, y = 0, z = 0, yaw = 0; //target location
  if(argc < 4)
    {
      return -1; 
    }else
    {
      x = atof(argv[1]); y = atof(argv[2]); z = atof(argv[3]); yaw = atof(argv[4]);
      ROS_INFO("x: %f, y: %f, z: %f, yaw: %f", x,y,z,yaw);
    }

  ROS_INFO("Welcome to CERLAB's Path Following Drone...!");
  ROS_INFO("Ready to receive commands...");
  
  while(ros::ok())
    { 
      temp32 = getchar();
      switch(temp32)
	{
	case 'C':
	  pos = pf.getCurrentPosition();
	  ROS_INFO("X: %f, Y: %f, Z: %f", pos.x, pos.y, pos.z);
	  flag = 0;
	  break;
	case 't':
	  pf.drone->takeoff();
	  pf.build_reduce_height(3.0);
	  flag = 0;
	  break;
	case 'l':
	  pf.landing();
	  flag = 1;
	case 'g':
	  pf.gohome();
	  flag = 1;
	  break;
	case 'o':
	  pf.obtain_control();
	  flag = 1;
	  break;
	case 'r':
	  pf.release_control();
	  flag = 1;
	  break;
	case 's':
	  pos = pf.getCurrentPosition();
	  ROS_INFO("X: %f, Y: %f, Z: %f", pos.x, pos.y, pos.z);
	  pf.stablize_flight(pos);
	  break;
	case 'n':
	  pf.navigate_to(x,y,z,yaw);
	  break;
	case 'b':
	  pf.track_bezier_curve(pts);
	case 'y':
	  ROS_INFO("Hi");
	  pf.test_yaw(yaw);
	  break;
	  }
     ros::spinOnce();
    }
  
 return 0;
}


