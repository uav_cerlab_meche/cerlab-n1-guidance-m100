#include <stdio.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <dji_sdk/dji_drone.h>

#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <guidance/OffsetData.h>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>


using namespace cv;
using namespace std;

int edgeThresh = 1;
int lowThreshold = 50;
int kernel_size = 3;
char key = 0;
int h_param = 50;

#define WIDTH 320
#define HEIGHT 240
#define IMAGE_SIZE (HEIGHT * WIDTH)
#define AS_PI 3.141590118

ros::Publisher offset_data_pub;

std::vector<Point2f> compute_slopes(std::vector<Vec2f> &lines, double &mean_ag1, double &mean_ag2)
{
  Point2f pt11, pt12, pt21, pt22;
  pt11.x = 0;
  pt11.y = 0;
  pt12.x = 0;
  pt12.y = 0;
  pt21.x = 0;
  pt21.y = 0;
  pt22.x = 0;
  pt22.y = 0;

  mean_ag1 = 0; mean_ag2 = 0;
  std::vector<Point2f> points;
  points.push_back(pt11);
  points.push_back(pt12);
  points.push_back(pt21);
  points.push_back(pt22);

  int flag1 = 0, flag2 = 0;

  for(size_t i=0; i<lines.size(); i++)
    {
      float rho = lines[i][0], theta = lines[i][1];
      Point2f pt1, pt2;
      double a = cos(theta), b = sin(theta);
      double x0 = a*rho, y0 = b*rho;
      pt1.x = cvRound(x0 + 1000*(-b));
      pt1.y = cvRound(y0 + 1000*(a));
      pt2.x = cvRound(x0 - 1000*(-b));
      pt2.y = cvRound(y0 - 1000*(a));
      double thresag = atan2((pt1.y-pt2.y),(pt1.x-pt2.x));
      // line(dst, pt1, pt2, Scalar(255,0,0), 1, CV_AA);
      if(fabs(thresag) > 15*(CV_PI/180) && fabs(thresag) < 165*(CV_PI/180))
	{
	  // line(dst, pt1, pt2, Scalar(255,0,0), 1, CV_AA);
	   if(flag1 == 0 && thresag > 0)
	    {
	      mean_ag1 = thresag;
	      points[0].x = pt1.x;
	      points[0].y = pt1.y;
	      points[1].x = pt2.x;
	      points[1].y = pt2.y;
	      flag1 = 1;

	    }else if(thresag > 0 && fabs(thresag) < 1.1*fabs(mean_ag1) && fabs(thresag) > 0.8*fabs(mean_ag1) && flag1 == 1)
	    {
	      mean_ag1 = (mean_ag1 + thresag)/2;
	      points[0].x = (points[0].x + pt1.x)/2;
	      points[0].y = (points[0].y + pt1.y)/2;
	      points[1].x = (points[1].x + pt2.x)/2;
	      points[1].y = (points[1].y + pt2.y)/2;
	    }

	   if( flag2 == 0 && thresag < 0)
	    {
	      mean_ag2 = thresag;
	      points[2].x = pt1.x;
	      points[2].y = pt1.y;
	      points[3].x = pt2.x;
	      points[3].y = pt2.y;
	      flag2 = 1;

	    }else if(thresag < 0 && fabs(thresag) < 1.1*fabs(mean_ag2) && fabs(thresag) > 0.8*fabs(mean_ag2) && flag2 == 1)
	    {
	      mean_ag2 = (mean_ag2 + thresag)/2;
	      points[2].x = (points[2].x + pt1.x)/2;
	      points[2].y = (points[2].y + pt1.y)/2;
	      points[3].x = (points[3].x + pt2.x)/2;
	      points[3].y = (points[3].y + pt2.y)/2;
	    }
	}
    }

  return points;
}

std::vector<Vec2f> compute_Hough(Mat detected_edges)
{
 
  std::vector<Vec2f> lines;
  for(int i = 0; i < (detected_edges.rows/2); i++)
    {
      for(int j = 0; j < (detected_edges.cols); j++)
	{
	  detected_edges.at<double>(i,j) = 0;
	}
    } 

  HoughLines(detected_edges, lines, 1, CV_PI/180, h_param);
  // ROS_INFO("Size: %d",(int)lines.size());

  return lines;
  
}

void imageReceived_right(const sensor_msgs::ImageConstPtr& msg)
{
  cv_bridge::CvImagePtr cv_im;
  
  try
    {
      cv_im = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
    }
  catch(cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
  
  Mat src = cv_im->image;
  imshow("right_image", src);
  Mat blurred_image;
  blur(src, blurred_image, Size(3,3));
  Mat detected_edges(blurred_image.rows, blurred_image.cols, blurred_image.depth());
  Mat new_detection(blurred_image.rows, blurred_image.cols, blurred_image.depth());
  Mat dst(blurred_image.rows, blurred_image.cols, blurred_image.depth());
  int ratio = 3;

  detected_edges = Scalar::all(0);
  new_detection = Scalar::all(0);
  dst = Scalar::all(0);

  std::vector<Vec2f> lines;
  std::vector<Point2f> points;
  
  double mean_ag1 = 0, mean_ag2 = 0;
  int count = 0,flag = 0;
  while( mean_ag1 == 0 || mean_ag2 == 0)
    {
      Canny(blurred_image, new_detection, lowThreshold-10*count, (lowThreshold-10*count)*ratio, kernel_size);
      detected_edges = detected_edges + new_detection;
      if(count == 0)
      {
			dst = detected_edges;
	  }
      count++;
      lines = compute_Hough(detected_edges);
      points = compute_slopes(lines, mean_ag1, mean_ag2);

      if(count > 2)
		{
			
			{
				break;
			}
		}
    }
    
 
  ROS_INFO("count: %d", count);
  line(dst, points[0], points[1], Scalar(255,0,0), 1, CV_AA);
  line(dst, points[2], points[3], Scalar(255,0,0), 1, CV_AA);
  imshow("subscribed image_right", dst);

  double x_l = 0, x_r = 0;
  
  x_l = (((double)src.rows - points[0].y)/tan(mean_ag1)) + points[0].x;
  x_r = (((double)src.rows - points[2].y)/tan(mean_ag2)) + points[2].x;

  double c1 = 0, c2 = 0;
  c1 = points[0].y - tan(mean_ag1)*points[0].x;
  c2 = points[2].y - tan(mean_ag2)*points[2].x;

  Point2f ptct1, ptct2;
  ptct1.x = (c1 - c2)/(tan(mean_ag2) - tan(mean_ag1));
  ptct1.y = tan(mean_ag1)*ptct1.x + c1;

  ptct2.x = (x_r + x_l)/2;
  ptct2.y = 240;
  guidance::OffsetData p;
  p.header.frame_id = "guidance";
  p.header.stamp = ros::Time::now();
  p.slope_l = mean_ag1;
  p.slope_r = mean_ag2;
  p.dx_l = x_l;
  p.dx_r = x_r;
  p.c_point_x = ptct1.x;
  p.c_point_y = ptct1.y;
  p.yaw = -(( AS_PI/2) - (atan2((ptct2.y - ptct1.y),(ptct2.x - ptct1.x)))); 
  if(isnan(p.yaw) || isinf(p.yaw) || p.c_point_y == 0 || mean_ag1 == 0 || mean_ag2 == 0)
    {
      p.reliability = 0;

    }else
    {
      p.reliability = 1;
    }
  offset_data_pub.publish(p);

  key =  waitKey(3);  
 }


int main(int argc, char **argv)
{
  
  ros::init(argc,argv,"dynamicHough");
  ros::NodeHandle nh;
  offset_data_pub = nh.advertise<guidance::OffsetData>("/guidance/posoffset", 1);
    {
       if(key == 'q')
	{
	  return 0;
	}
       printf("key : %c", key);
       // ros::Subscriber sub1 = nh.subscribe("/guidance/left_image",1,&imageReceived_left);
  
	  ros::Subscriber sub2 = nh.subscribe("/guidance/right_image",1,&imageReceived_right);

  ros::spin();
    }
  

  return 0;
}
