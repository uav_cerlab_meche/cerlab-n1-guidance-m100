#include <iostream>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

std::vector<cv::Mat> getFilterBank()
{
  std::vector<cv::Mat> filterBank;
  double scales[5] = {1, 2, 4, 8, 8*sqrt(2)};

  //gaussian filter
  for(int i = 0; i < 5; i++)
    {
       Mat kernalX = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernalY = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernal = kernalX * kernalY.t();
       filterBank.push_back(kernal);
    }

  for(int k = 0; k < 5; k++)
    {
      double WinSize = 2*ceil(scales[k]*2.5)+1;
      cv::Mat kernel(WinSize,WinSize,CV_64F);
      int rows = kernel.rows;
      int cols = kernel.cols;
      double halfSize = floor( WinSize / 2.0); 
      for (size_t i=0; i<rows;i++)
	{
	  for (size_t j=0; j<cols;j++)
	  { 
	    double x = (double)j - halfSize;
	    double y = (double)i - halfSize;
	    kernel.at<double>(j,i) = - (1.0 /(CV_PI*pow(scales[k],4))) * (1 - ((x*x+y*y)/(2*scales[k]*scales[k])))* exp(-((x*x + y*y) / (2*scales[k]*scales[k])));
	  }
	}
      filterBank.push_back(kernel);
    }
  
  //gaussian + xderivate filter
  for(int i = 0; i < 5; i++)
    {
       Mat kernalX = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernalY = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernal = kernalX * kernalY.t();
      
       Mat dGaussian(2*ceil(scales[i]*2.5)+1, 2*ceil(scales[i]*2.5)+1, CV_64F);
       Mat dx(1,3,CV_64F);
       dx.at<double>(0,0) = -1;
       dx.at<double>(0,1) = 0;
       dx.at<double>(0,2) = 1;
       Point anchor(dx.cols - dx.cols/2 - 1, dx.rows - dx.rows/2 - 1);
       filter2D(kernal, dGaussian, CV_64F, dx, anchor, 0, BORDER_CONSTANT);
       filterBank.push_back(dGaussian);
    }
  // gaussian + yderivative
  for(int i = 0; i < 5; i++)
    {
       Mat kernalX = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernalY = getGaussianKernel(2*ceil(scales[i]*2.5)+1, scales[i], CV_64F);
       Mat kernal = kernalX * kernalY.t();
      
       Mat dGaussian(2*ceil(scales[i]*2.5)+1, 2*ceil(scales[i]*2.5)+1, CV_64F);
       Mat dx(1,3,CV_64F);
       dx.at<double>(0,0) = -1;
       dx.at<double>(0,1) = 0;
       dx.at<double>(0,2) = 1;
       Point anchor(dx.cols - dx.cols/2 - 1, dx.rows - dx.rows/2 - 1);
       filter2D(kernal, dGaussian, CV_64F, dx, anchor, 0, BORDER_CONSTANT);
       filterBank.push_back(dGaussian.t());
    }
  return filterBank;
  
}


cv::Mat extractFilterResponse(cv::Mat I)
{
  
  // printf("Row: %d, Col: %d\n",I.rows,I.cols);
  // cout<<I.channels()<<endl;
   if(I.channels() != 3)
    {
      std::vector<cv::Mat> channels;
      channels.push_back(I);
      channels.push_back(I);
      channels.push_back(I);
      cv::merge(channels, I);
    }
   
   int pixelCount = I.rows*I.cols;
   std::vector<cv::Mat> filterBank = getFilterBank();
   cv::Mat filterResponses = cv::Mat::zeros(pixelCount, (int)filterBank.size()*3, CV_64F);
   // cout<<filterResponses<<endl;
   // printf("Row: %d, Col: %d", filterResponses.rows, filterResponses.cols);
   Mat BGR_3[3];
   split(I, BGR_3);
   int count = 0; 
   for(int i = 0; i < filterBank.size(); i++)
     {
       for(int j = 0; j < I.channels(); j++)
	 {
	   Mat F;
	   Point anchor(filterBank[i].cols - filterBank[i].cols/2 - 1, filterBank[i].rows - filterBank[i].rows/2 - 1);
	   filter2D(BGR_3[j], F, CV_64F, filterBank[i], anchor, 0, BORDER_CONSTANT);
	   Mat F_new = Mat(F).reshape(0,1);
	   F_new = F_new.t();
	   for(int k = 0 ; k < pixelCount; k++)
	     {
	       filterResponses.at<double>(k, count) = F_new.at<double>(k,0);
	       //cout<<F_new.at<double>(0,k)<<" ";
	     }
	   count++;
	 }
     }

   // cout<<filterResponses<<endl;
   
   return filterResponses;
}


/* Returns a list of files in a directory (except the ones that begin with a dot) */

void readFilenames(std::vector<string> &filenames, const string &directory)
{
#ifdef WINDOWS
    HANDLE dir;
    WIN32_FIND_DATA file_data;

    if ((dir = FindFirstFile((directory + "/*").c_str(), &file_data)) == INVALID_HANDLE_VALUE)
        return; /* No files found */

    do {
        const string file_name = file_data.cFileName;
        const string full_file_name = directory + "/" + file_name;
        const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

        if (file_name[0] == '.')
            continue;

        if (is_directory)
            continue;

        filenames.push_back(full_file_name);
    } while (FindNextFile(dir, &file_data));

    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(directory.c_str());
    while ((ent = readdir(dir)) != NULL) {
        const string file_name = ent->d_name;
        const string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.')
            continue;

        if (stat(full_file_name.c_str(), &st) == -1)
            continue;

        const bool is_directory = (st.st_mode & S_IFDIR) != 0;

        if (is_directory)
            continue;

//        filenames.push_back(full_file_name); // returns full path
        filenames.push_back(file_name); // returns just filename
    }
    closedir(dir);
#endif
} // GetFilesInDirectory

void getRandomNumbers(long int num[], int size)
{
  srand(time(NULL));
  
  for(int j = 0; j < size; j++)
    {
      num[j] = j;
    }
  int  max = size - 1;
  
    while(max >= 0 )
      {
	int index = rand() % size;
	int temp = num[index];
	num[index] = num[max];
	num[max] = temp ; 
	max--;
      }
 
}

void createDictionary()
{
  string folder = "../images/";
    vector<string> filenames;

    readFilenames(filenames, folder);
    cout<<filenames.size()<<endl;
    int NumPix = 20000;
    // getRandomNumbers(pix, NumPix);
    
    int t = 0, k;  // counters
      
    std::vector<cv::Mat> filterBank = getFilterBank();
    cv::Mat allFilterResponses = Mat::zeros((int)filenames.size()*NumPix, (int)filterBank.size()*3, CV_64F);
    
    for(size_t i = 0; i < filenames.size(); ++i)
	  {
	    Mat src = imread(folder + filenames[i], CV_LOAD_IMAGE_COLOR); 

	    if(!src.data)
	      {
		cerr<< "File is corrupted!!!" << endl;
	      }else
	      {
		 Mat filterResponses = extractFilterResponse(src);
		 long int pix[(int)filterResponses.rows];	
		 getRandomNumbers(pix, (int)filterResponses.rows);
		 k = 0;
		 for(int j = t; j < NumPix*(i+1); j++)
		   {
		     for(int m = 0; m < 3*filterBank.size(); m++)
		       {
			 allFilterResponses.at<double>(j,m) = filterResponses.at<double>(pix[k],m);
		       }
		     k = k+1;
		   }	
		 t = NumPix*(i+1);
		 printf("Trained image: %d\n", (int)(i+1));
	      }
	    // imshow("image", src);
	    // waitKey(20);
	  }

    allFilterResponses.convertTo(allFilterResponses, CV_32F);
    TermCriteria TC;
    TC = TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.01);
    Mat labels;
    int K = 20;
    int attempts = 10;
    Mat centres;
    kmeans(allFilterResponses, K, labels, TC, attempts, KMEANS_RANDOM_CENTERS, centres);
    cout<<centres<<endl;
    cout<<centres.rows<<" "<<centres.cols<<endl;
    // cout<<allFilterResponses<<endl;
     
    
}




int main(int argc, char** argv)
{
  string in = argv[1];
  Mat I = cv::imread(in, CV_LOAD_IMAGE_COLOR);
  printf("Image_Row: %d, Image_Col: %d\n",I.rows,I.cols);
  //cout<<I.channels()<<endl;
  if(I.channels() != 3)
    {
      std::vector<cv::Mat> channels;
      channels.push_back(I);
      channels.push_back(I);
      channels.push_back(I);
      cv::merge(channels, I);
    }

  Mat filterResponses = extractFilterResponse(I);
  // cout<<filterResponses<<endl;
 // std::vector<cv::Mat> filterBank = getFilterBank();
  
  Mat I_new = Mat(I).reshape(0,1);
  I_new = I_new.t();
  printf("I_newrow: %d, I_newcol: %d\n",I_new.rows, I_new.cols);
  printf("FR_rows: %d, FR_cols: %d\n", filterResponses.rows, filterResponses.cols);
   createDictionary();
  
   /* int numpix = 40000;
  long int pix[filterResponses.rows];
  getRandomNumbers(pix, numpix);
  
  for(int i = 0; i < numpix; i++)
    {
      printf("%d ", pix[i]);
    }
    printf("\n");*/

  return 0;
}
