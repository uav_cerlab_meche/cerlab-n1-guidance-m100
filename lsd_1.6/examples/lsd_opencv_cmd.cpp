#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>

#include "lsd_opencv.hpp"

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		std::cout << "lsd_opencv_cmd [in] [out]" << std::endl
			<< "\tin - input image" << std::endl
			<< "\tout - output containing a line segment at each line [x1, y1, x2, y2, width, p, -log10(NFA)]" << std::endl;
		return false;
	}

	std::string in = argv[1];
	std::string out = argv[2];

	Mat image = imread(in, CV_LOAD_IMAGE_GRAYSCALE);

	// LSD call
	std::vector<Vec4f> lines;
    std::vector<double> width, prec;
	Ptr<LineSegmentDetector> lsd = createLineSegmentDetectorPtr();

    double start = double(getTickCount());
    lsd->detect(image, lines, width, prec);
    double duration_ms = (double(getTickCount()) - start) * 1000 / getTickFrequency();

    std::cout << lines.size() <<" line segments found. For " << duration_ms << " ms." << std::endl;

	//Save to file
	ofstream segfile;
  	segfile.open(out.c_str());
    for (unsigned int i = 0; i < lines.size(); ++i)
    {
		cout << '\t' << "B: " << lines[i][0] << " " << lines[i][1]
		<< " E: " << lines[i][2] << " " << lines[i][3]
		<< " W: " << width[i]
		<< " P:" << prec[i] << endl;
		segfile << '\t' << "B: " << lines[i][0] << " " << lines[i][1]
		<< " E: " << lines[i][2] << " " << lines[i][3]
		<< " W: " << width[i]
		<< " P:" << prec[i] << endl;
    }
	segfile.close();
	
	std::vector<Vec4f> lines1;
	std::vector<double> width1;
	int count = 0;
	Mat img1(Size(image.cols, image.rows), CV_8UC1, Scalar(255));
	Point2f pt1,pt2;
	Mat subimg = image(Range(210,240), Range(226,256));
	//	cout<<subimg<<endl;
	cout<<image.rows<<" "<< image.cols<<endl;
	//	cout<<image<<endl;
	for(unsigned int i = 0; i < lines.size(); i++)
	  {
	    double ang = atan2(lines[i][1] - lines[i][3], lines[i][0] - lines[i][2]);
	    //printf("ang: %f\n", ang);
	  
	    
	     if(fabs(ang) > 0.6726 && fabs(ang) < 2.1)
	      {
		if(lines[i][1] > image.cols/2 || lines[i][3] > image.cols/2)
		  {
		    // if((lines[i][0] > (image.rows/2)-20 && lines[i][0] < (image.rows/2)+20)||(lines[i][2] > (image.rows/2)-20 && lines[i][2] < (image.rows/2)+20))
		    if(image.at<uchar>(lines[i][0],lines[i][1]) > 180  && image.at<uchar>(lines[i][2],lines[i][3]) > 180)
		      {
		      }else
		      {
			 pt1.x = lines[i][0]; pt1.y = lines[i][1];
			 pt2.x = lines[i][2]; pt2.y = lines[i][3];
			 line(img1, pt1, pt2, Scalar(0,0,0), width[i], CV_AA);
		      }
		   
		  } 
	      }
	  }

	imshow("seg_img",img1);
	imshow("or_img", image);
	imshow("sub_img",subimg);
	waitKey(0);
	return 0;
}
