# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/cerlab/catkin_ws/src/lsd_1.6/examples/lsd_wrap_cmd.cpp" "/home/cerlab/catkin_ws/src/lsd_1.6/build/examples/CMakeFiles/lsd_wrap_cmd.dir/lsd_wrap_cmd.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/cerlab/catkin_ws/src/lsd_1.6/build/src/CMakeFiles/lsd_wrap.dir/DependInfo.cmake"
  "/home/cerlab/catkin_ws/src/lsd_1.6/build/src/CMakeFiles/lsd.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../libs"
  "../include"
  "/usr/include/opencv"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
