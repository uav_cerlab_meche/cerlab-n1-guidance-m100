# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/cerlab/catkin_ws/src/lsd_1.6

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/cerlab/catkin_ws/src/lsd_1.6/build

# Include any dependencies generated for this target.
include examples/CMakeFiles/lsd_opencv_example.dir/depend.make

# Include the progress variables for this target.
include examples/CMakeFiles/lsd_opencv_example.dir/progress.make

# Include the compile flags for this target's objects.
include examples/CMakeFiles/lsd_opencv_example.dir/flags.make

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o: examples/CMakeFiles/lsd_opencv_example.dir/flags.make
examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o: ../examples/lsd_opencv_example.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/cerlab/catkin_ws/src/lsd_1.6/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/examples && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o -c /home/cerlab/catkin_ws/src/lsd_1.6/examples/lsd_opencv_example.cpp

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.i"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/examples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/cerlab/catkin_ws/src/lsd_1.6/examples/lsd_opencv_example.cpp > CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.i

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.s"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/examples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/cerlab/catkin_ws/src/lsd_1.6/examples/lsd_opencv_example.cpp -o CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.s

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.requires:
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.requires

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.provides: examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.requires
	$(MAKE) -f examples/CMakeFiles/lsd_opencv_example.dir/build.make examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.provides.build
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.provides

examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.provides.build: examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o

# Object files for target lsd_opencv_example
lsd_opencv_example_OBJECTS = \
"CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o"

# External object files for target lsd_opencv_example
lsd_opencv_example_EXTERNAL_OBJECTS =

../bin/lsd_opencv_example: examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o
../bin/lsd_opencv_example: examples/CMakeFiles/lsd_opencv_example.dir/build.make
../bin/lsd_opencv_example: ../lib/liblsd_opencv.a
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/lsd_opencv_example: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/lsd_opencv_example: examples/CMakeFiles/lsd_opencv_example.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../../bin/lsd_opencv_example"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/examples && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/lsd_opencv_example.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/CMakeFiles/lsd_opencv_example.dir/build: ../bin/lsd_opencv_example
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/build

examples/CMakeFiles/lsd_opencv_example.dir/requires: examples/CMakeFiles/lsd_opencv_example.dir/lsd_opencv_example.cpp.o.requires
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/requires

examples/CMakeFiles/lsd_opencv_example.dir/clean:
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/examples && $(CMAKE_COMMAND) -P CMakeFiles/lsd_opencv_example.dir/cmake_clean.cmake
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/clean

examples/CMakeFiles/lsd_opencv_example.dir/depend:
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/cerlab/catkin_ws/src/lsd_1.6 /home/cerlab/catkin_ws/src/lsd_1.6/examples /home/cerlab/catkin_ws/src/lsd_1.6/build /home/cerlab/catkin_ws/src/lsd_1.6/build/examples /home/cerlab/catkin_ws/src/lsd_1.6/build/examples/CMakeFiles/lsd_opencv_example.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/CMakeFiles/lsd_opencv_example.dir/depend

