# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/cerlab/catkin_ws/src/lsd_1.6

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/cerlab/catkin_ws/src/lsd_1.6/build

# Include any dependencies generated for this target.
include tests/CMakeFiles/visual_test.dir/depend.make

# Include the progress variables for this target.
include tests/CMakeFiles/visual_test.dir/progress.make

# Include the compile flags for this target's objects.
include tests/CMakeFiles/visual_test.dir/flags.make

tests/CMakeFiles/visual_test.dir/visual_test.cpp.o: tests/CMakeFiles/visual_test.dir/flags.make
tests/CMakeFiles/visual_test.dir/visual_test.cpp.o: ../tests/visual_test.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/cerlab/catkin_ws/src/lsd_1.6/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object tests/CMakeFiles/visual_test.dir/visual_test.cpp.o"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/tests && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/visual_test.dir/visual_test.cpp.o -c /home/cerlab/catkin_ws/src/lsd_1.6/tests/visual_test.cpp

tests/CMakeFiles/visual_test.dir/visual_test.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/visual_test.dir/visual_test.cpp.i"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/tests && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/cerlab/catkin_ws/src/lsd_1.6/tests/visual_test.cpp > CMakeFiles/visual_test.dir/visual_test.cpp.i

tests/CMakeFiles/visual_test.dir/visual_test.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/visual_test.dir/visual_test.cpp.s"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/tests && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/cerlab/catkin_ws/src/lsd_1.6/tests/visual_test.cpp -o CMakeFiles/visual_test.dir/visual_test.cpp.s

tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.requires:
.PHONY : tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.requires

tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.provides: tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/visual_test.dir/build.make tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.provides.build
.PHONY : tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.provides

tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.provides.build: tests/CMakeFiles/visual_test.dir/visual_test.cpp.o

# Object files for target visual_test
visual_test_OBJECTS = \
"CMakeFiles/visual_test.dir/visual_test.cpp.o"

# External object files for target visual_test
visual_test_EXTERNAL_OBJECTS =

../bin/visual_test: tests/CMakeFiles/visual_test.dir/visual_test.cpp.o
../bin/visual_test: tests/CMakeFiles/visual_test.dir/build.make
../bin/visual_test: ../lib/liblsd_opencv.a
../bin/visual_test: ../lib/liblsd_wrap.a
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_videostab.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_ts.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_superres.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_stitching.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_ocl.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_gpu.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_contrib.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_photo.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_legacy.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_video.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_objdetect.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_ml.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_calib3d.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_features2d.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_highgui.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_imgproc.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_flann.so.2.4.8
../bin/visual_test: /usr/lib/x86_64-linux-gnu/libopencv_core.so.2.4.8
../bin/visual_test: ../lib/liblsd.a
../bin/visual_test: tests/CMakeFiles/visual_test.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../../bin/visual_test"
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/tests && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/visual_test.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
tests/CMakeFiles/visual_test.dir/build: ../bin/visual_test
.PHONY : tests/CMakeFiles/visual_test.dir/build

tests/CMakeFiles/visual_test.dir/requires: tests/CMakeFiles/visual_test.dir/visual_test.cpp.o.requires
.PHONY : tests/CMakeFiles/visual_test.dir/requires

tests/CMakeFiles/visual_test.dir/clean:
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build/tests && $(CMAKE_COMMAND) -P CMakeFiles/visual_test.dir/cmake_clean.cmake
.PHONY : tests/CMakeFiles/visual_test.dir/clean

tests/CMakeFiles/visual_test.dir/depend:
	cd /home/cerlab/catkin_ws/src/lsd_1.6/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/cerlab/catkin_ws/src/lsd_1.6 /home/cerlab/catkin_ws/src/lsd_1.6/tests /home/cerlab/catkin_ws/src/lsd_1.6/build /home/cerlab/catkin_ws/src/lsd_1.6/build/tests /home/cerlab/catkin_ws/src/lsd_1.6/build/tests/CMakeFiles/visual_test.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : tests/CMakeFiles/visual_test.dir/depend

