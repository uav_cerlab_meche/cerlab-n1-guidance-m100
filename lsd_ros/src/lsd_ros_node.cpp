#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include "lsd_opencv.hpp"
#include <vector>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
  ros::init(argc, argv,"lsd_node");
  ros::NodeHandle nh;
  cv::Mat image = imread("left01.jpg",CV_LOAD_IMAGE_GRAYSCALE);
    std::vector<Vec4i> lines;
    std::vector<double> width, prec;
    // Ptr<LineSegmentDetector> lsd = createLineSegmentDetectorPtr();

     double start = double(cv::getTickCount());
    // lsd->detect(image, lines, width, prec);
     double duration_ms = (double(cv::getTickCount()) - start) * 1000 / cv::getTickFrequency();
  // Ptr<LineSegmentDetector> lsd  = createLineSegmentDetectorPtr();

  while(ros::ok())
    {
      ros::spinOnce();
    }
  return 0;
}
